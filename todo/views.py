from urllib import request
from django.shortcuts import render
from rest_framework import viewsets
from .serializers import TodoSerializer
from django.shortcuts import render,redirect
from .models import Todo

# Create your views here.

class TodoView(viewsets.ModelViewSet):
    serializer_class = TodoSerializer

    def index(request):
        todo = Todo.objects.all()
        if request.method == "POST": 
            if "addTask" in request.POST: 
                title = request.POST["title"] 
                date = str(request.POST["date"]) 
                description = request.POST["description"]  
                do = Todo(title=title, description=description, due_date=date)
                do.save()
                return redirect("/")
                return render(request, 'index.html', {"todos": todos})

