from email.policy import default
from django.db import models
from django.utils import timezone

# Create your models here.

class Todo(models.Model):
    title = models.CharField(max_length=120)
    description = models.TextField()
    completed = models.BooleanField(default=False)
    created = models.DateField(default=timezone.now().strftime("%Y-%m-%d %H:%I"))
    due_date = models.DateField(default=timezone.now().strftime("%Y-%m-%d %H:%I"))
    class Meta:
        ordering = ["-created"]

    def _str_(self):
        return self.title
